package com.yuktamedia.analytics;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.google.ads.interactivemedia.v3.api.AdErrorEvent;
import com.google.ads.interactivemedia.v3.api.AdEvent;
import com.google.ads.interactivemedia.v3.api.AdsLoader;
import com.google.ads.interactivemedia.v3.api.AdsManagerLoadedEvent;
import com.google.gson.Gson;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class AdsDataCollector implements  AdEvent.AdEventListener, AdErrorEvent.AdErrorListener, AdsLoader.AdsLoadedListener  {
    Context context;
    Gson gson = new Gson();

    @Override
    public void onAdError(AdErrorEvent adErrorEvent) {

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onAdEvent(AdEvent event) {
        Map<String,String> admap = event.getAdData();
        Log.e("Event.getAdData","Ads_Data"+event.toString());
//        convertAdsData(event.toString());
        switch (event.getType()) {
            case ALL_ADS_COMPLETED:
                Log.e("AdData_SIze","ALL_ADS_COMPLETED");
                String rawdata = event.toString();
                String array =  gson.toJson(rawdata);
//                Analytics.with(context).trackJSON(admap);
                Analytics.with(context).trackJSON(event.toString());
                break;// Do nothing
            case AD_BREAK_FETCH_ERROR:
                Log.e("AdData_SIze","AD_BREAK_FETCH_ERROR");
//                Analytics.with(context).track(event.toString());
                break;
            case CLICKED:

                int a = admap.size();
                String rawdataClicked = event.toString();
                String clickedarray =  gson.toJson(rawdataClicked);
                Analytics.with(context).track(clickedarray);
                Log.e("AdData_SIze", String.valueOf(a));
                Analytics.with(context).track(event.toString());
                break;
            case COMPLETED:
                Log.e("AdData_SIze","COMPLETED");
                Analytics.with(context).track(event.toString());
                Analytics.with(context).trackJSON(event.toString());
                break;
            case CUEPOINTS_CHANGED:
                Log.e("AdData_SIze","CUEPOINTS_CHANGED");
                Analytics.with(context).trackJSON(event.toString());
                break;
            case CONTENT_PAUSE_REQUESTED:
                Log.e("AdData_SIze","CONTENT_PAUSE_REQUESTED");
                Analytics.with(context).trackJSON(event.toString());
                break;
            case CONTENT_RESUME_REQUESTED:
                Log.e("AdData_SIze","CONTENT_RESUME_REQUESTED");
                Analytics.with(context).trackJSON(event.toString());
                break;
            case FIRST_QUARTILE:
                String rawdataFirstQuartlie = event.toString();
                Log.e("AdData_SIze","FIRST_QUARTILE");
                String firstquartilearray =  gson.toJson(rawdataFirstQuartlie);
                Analytics.with(context).trackJSON(rawdataFirstQuartlie);
//                Analytics.with(context).trackJSONJSON(admap);
                break;
            case LOG:

                Log.e("AdData_SIze","LOG");
//                Analytics.with(context).trackJSON(event.toString());
                Analytics.with(context).trackJSON(event.toString());
                break;
            case AD_BREAK_READY:
                Log.e("AdData_SIze","AD_BREAK_READY");
                Analytics.with(context).trackJSON(event.toString());
                break;
            case MIDPOINT:
                Log.e("AdData_SIze","MIDPOINT");
                Analytics.with(context).trackJSON(event.toString());
                break;
            case PAUSED:
                Log.e("AdData_SIze","PAUSED");
                Analytics.with(context).trackJSON(event.toString());
                break;
            case RESUMED:
                Log.e("AdData_SIze","RESUMED");
                Analytics.with(context).trackJSON(event.toString());
                break;
            case SKIPPABLE_STATE_CHANGED:
                Log.e("AdData_SIze","SKIPPABLE_STATE_CHANGED");
                Analytics.with(context).trackJSON(event.toString());
                break;
            case SKIPPED:
                Log.e("AdData_SIze","SKIPPED");
                Analytics.with(context).trackJSON(event.toString());
                break;
            case STARTED:
                Log.e("AdData_SIze","STARTED");
                Analytics.with(context).trackJSON(event.toString());
            case TAPPED:
                Log.e("AdData_SIze","TAPPED");
                Analytics.with(context).trackJSON(event.toString());
                break;
            case ICON_TAPPED:
                Log.e("AdData_SIze","ICON_TAPPED");
                Analytics.with(context).trackJSON(event.toString());
                break;
            case ICON_FALLBACK_IMAGE_CLOSED:
                Log.e("AdData_SIze","ICON_FALLBACK_IMAGE_CLOSED");
                Analytics.with(context).trackJSON(event.toString());
//                Analytics.with(context).trackJSONJSON(admap);
                break;
            case THIRD_QUARTILE:
                Log.e("AdData_SIze","THIRD_QUARTILE");
                Map<String,String> admap1 = event.getAdData();
                Analytics.with(context).trackJSON(event.toString());
//                Analytics.with(context).trackJSONJSON(admap1);
                break;
            case LOADED:
                Log.e("AdData_SIze","LOADED");
                event.getAdData();
                Analytics.with(context).trackJSON(event.toString());
                break;
            case AD_PROGRESS:
                Log.e("AdData_SIze","AD_PROGRESS");
                Analytics.with(context).trackJSON(event.toString());
                break;
            case AD_BUFFERING:
                Log.e("AdData_SIze","AD_BUFFERING");
                Analytics.with(context).trackJSON(event.toString());
                break;
            case AD_BREAK_STARTED:
                Log.e("AdData_SIze","AD_BREAK_STARTED");
                Analytics.with(context).trackJSON(event.toString());
                break;
            case AD_BREAK_ENDED:
                Log.e("AdData_SIze","AD_BREAK_ENDED");
                Analytics.with(context).trackJSON(event.toString());
                break;
            case AD_PERIOD_STARTED:
                Log.e("AdData_SIze","AD_PERIOD_STARTED");
                Analytics.with(context).trackJSON(event.toString());
                break;
            case AD_PERIOD_ENDED:
                Log.e("AdData_SIze","AD_PERIOD_ENDED");
                Analytics.with(context).trackJSON(event.toString());
                break;
            default:
                Log.e("AdData_SIze","Default");
                Log.e("Ads", event.getType().toString());
                break;
        }

    }

//    @RequiresApi(api = Build.VERSION_CODES.N)
//    void convertAdsData(String rawadsData){
//        Map<String,String> adsprimesort = Arrays.stream(rawadsData.split(","))
//                .map(s -> s.split("="))
//                .collect(Collectors.toMap(s -> s[0], s->  s[1]));
////        char [] arraydata = rawadsData.toCharArray();
//        for (int a = 0; a < adsprimesort.size(); a++) {
//            Log.e("Arrayval","Data is "+adsprimesort.get(a));
//
//        }
//    }

    @Override
    public void onAdsManagerLoaded(AdsManagerLoadedEvent adsManagerLoadedEvent) {

    }



}
